# Aplikacija - upute za pokretanje

Za backend: 

	- python3 je potreban
	- pip3 install flask requests
	
Za frontend:

1. Potrebno je imati instaliran npm (Node package manager) i angular-cli (koji se moze instalirati preko npm-a).
2. Pozicioniorati se u ./front.
3. Izvrsiti 'npm i'.
4. Pokrenuti aplikaciju naredbom 'ng serve'.
5. Otvoriti u browseru: 'localhost:4200'