import requests

REGISTER_URL = 'http://52.233.158.172/change/api/hr/account/register'
LOGIN_URL = 'http://52.233.158.172/change/api/hr/account/login'
TEAM_URL = 'http://52.233.158.172/change/api/hr/team/details/24'
SUBMIT_URL = 'http://52.233.158.172/change/api/hr/team/confirm?id=24&repository=https%3A%2F%2Fbitbucket.org%2Fantoniojuric%2Fbecari-changecode'

register_data = {
    'Teamname': 'Naziv_tima',
    'Password': 'ShifraZaChangeCode',
    'Members': [
        {
            'name': 'Dominik',
            'surname': 'Ivošević',
            'mail': 'dominik.ivosevic@fer.hr'
        },
        {
            'name': 'Marko',
            'surname': 'Jagodić',
            'mail': 'marko.jagodic@fer.hr'
        },
        {
            'name': 'Zvonimir',
            'surname': 'Jurelinac',
            'mail': 'zvonimir.jurelinac@fer.hr'
        },
        {
            'name': 'Antonio',
            'surname': 'Jurić',
            'mail': 'antonio.juric@fer.hr'
        }
    ]
}

login_data = {
    'Teamname': 'Naziv_tima',
    'Password': 'ShifraZaChangeCode'
}

headers = {
    # 'Host': '52.233.158.172',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    # 'Cache-Control': 'max-age=0'
}

auth_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    "X-Authorization": 'TmF6aXZfdGltYTo6'
}


def register():
    resp = requests.post(REGISTER_URL, json=register_data, headers=headers)
    print(resp.status_code, resp.content)


def login():
    resp = requests.post(LOGIN_URL, json=login_data, headers=headers)
    print(resp.status_code, resp.content)


def get_team():
    resp = requests.get(TEAM_URL, headers=auth_headers)
    print(resp.json())


def submit_repo():
    resp = requests.get(SUBMIT_URL, headers=auth_headers)
    print(resp.status_code, resp.content)


if __name__ == '__main__':
    submit_repo()
