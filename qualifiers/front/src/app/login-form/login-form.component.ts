import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LocaleServiceService } from '../locale-service.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  options: FormGroup;

  proxy_server_address = 'http://localhost:8000';
  api_server_address = 'http://52.233.158.172/change/api';

  REGISTER_URL = '/hr/account/register';
  LOGIN_URL = '/hr/account/login';
  TEAM_URL = '/hr/team/details/24';
  SUBMIT_URL = '/hr/team/confirm?id=24&repository=';

  constructor(fb: FormBuilder, private http: HttpClient, private localeService: LocaleServiceService) {
    this.options = fb.group({
      floatLabel: 'auto',
      teamname: '',
      password: ''
    });
  }

  public onSubmit() {
    this.http.post(this.proxy_server_address + this.LOGIN_URL,
      { TeamName: this.options.value.teamname, Password: this.options.value.password })
      .subscribe((data: string) => {
        console.log(data);
        const result = JSON.parse(data['Result']);
        localStorage.setItem('myAuth', result['AuthorizationToken']);
        localStorage.setItem('teamId', result['TeamId']);
      },
        (data) => {
          console.log(data);
        });
  }
}
