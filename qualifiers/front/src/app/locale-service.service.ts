import { Injectable } from '@angular/core';

@Injectable()
export class LocaleServiceService {

  isEnglish: string;

  public isEnglishCheck() {
    if (this.isEnglish !== 'en' && this.isEnglish !== 'hr') {
      this.isEnglish = localStorage.getItem('locale') === 'en' ? 'en' : 'hr';
    }
    return this.isEnglish === 'en';
  }

  constructor() { }
}
