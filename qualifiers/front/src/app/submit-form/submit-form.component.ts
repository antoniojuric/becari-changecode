import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LocaleServiceService } from '../locale-service.service';

@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.css']
})
export class SubmitFormComponent {
  options: FormGroup;

  proxy_server_address = 'http://localhost:8000';
  api_server_address = 'http://52.233.158.172/change/api';

  REGISTER_URL = '/hr/account/register';
  LOGIN_URL = '/hr/account/login';
  TEAM_URL = '/hr/team/details/24';

  constructor(fb: FormBuilder, private http: HttpClient, private localeService: LocaleServiceService) {
    this.options = fb.group({
      Teamname: '',
      Password: '',
      first: fb.group({
        name: '',
        surname: '',
        mail: ''
      }),
      second: fb.group({
        name: '',
        surname: '',
        mail: ''
      }),
      third: fb.group({
        name: '',
        surname: '',
        mail: ''
      }),
      fourth: fb.group({
        name: '',
        surname: '',
        mail: ''
      })
    });
  }

  public onSubmit() {
    console.log(this.options.value);
    const send = {
      Teamname: this.options.value['Teamname'],
      Password: this.options.value['Password'],
      Members: [
        this.options.value['first'],
        this.options.value['second'],
        this.options.value['third'],
        this.options.value['fourth']
      ]
    };
    console.log(send);

    this.http.post(this.proxy_server_address + this.LOGIN_URL, send)
      .subscribe((data: string) => {
        console.log(data);
        const result = JSON.parse(data['Result']);
        localStorage.setItem('myAuth', result['AuthorizationToken']);
        localStorage.setItem('teamId', result['TeamId']);
      },
      (data) => {
        console.log(data);
      });
  }
}
