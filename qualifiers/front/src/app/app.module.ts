import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { DemoMaterialModule } from './demo-material/demo-material.module';

import { AppComponent } from './app.component';
import { CdkTableModule } from '@angular/cdk/table';
import { SubmitFormComponent } from './submit-form/submit-form.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';
import { InfoComponent } from './info/info.component';
import { LocaleServiceService } from './locale-service.service';

const appRoutes: Routes = [
  { path: 'submit', component: SubmitFormComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'info', component: InfoComponent },
  { path: '**', component: SubmitFormComponent }
];

// const translations = require(`raw-loader!./../locale/messages.hr.xlf`);

@NgModule({
  declarations: [
    AppComponent,
    SubmitFormComponent,
    LoginFormComponent,
    InfoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    DemoMaterialModule,
    CdkTableModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
    LocaleServiceService
    // { provide: TRANSLATIONS, useValue: translations },
    // { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' },
    // { provide: LOCALE_ID, useValue: 'hr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
