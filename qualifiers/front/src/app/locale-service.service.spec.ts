import { TestBed, inject } from '@angular/core/testing';

import { LocaleServiceService } from './locale-service.service';

describe('LocaleServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocaleServiceService]
    });
  });

  it('should be created', inject([LocaleServiceService], (service: LocaleServiceService) => {
    expect(service).toBeTruthy();
  }));
});
