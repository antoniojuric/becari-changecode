import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { SubmitFormComponent } from './submit-form/submit-form.component';
import { LocaleServiceService } from './locale-service.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  changeLang(lang) {
    localStorage.setItem('locale', lang);
    this.localeService.isEnglish = lang;
  }

  constructor(private localeService: LocaleServiceService) {

  }
}
