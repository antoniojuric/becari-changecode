import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { LocaleServiceService } from '../locale-service.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  proxy_server_address = 'http://localhost:8000';
  api_server_address = 'http://52.233.158.172/change/api';

  TEAM_URL = '/hr/team/details/';
  REPOSITORY_URL = 'https://bitbucket.org/antoniojuric/becari-changecode';
  SUBMIT_URL = '/hr/team/confirm?id=24&repository=' + this.REPOSITORY_URL;

  result: any = { Members: [] };
  members: any = [];

  ngOnInit() {
    this.get(this.TEAM_URL + localStorage.getItem('teamId'));
  }

  public submit() {
    this.get(this.SUBMIT_URL);
  }

  constructor(private http: HttpClient, private localeService: LocaleServiceService) {
  }

  private get(url: string) {
    this.http.get(this.proxy_server_address + url,
      {
        headers: {
          'X-Authorization': localStorage.getItem('myAuth')
        }
      })
      .subscribe((data: string) => {
        console.log(data);
        this.result = JSON.parse(data['Result']);
        this.members = this.result['Members'];
        console.log(this.result);
      },
        (data) => {
          console.log(data);
        });
  }
}
