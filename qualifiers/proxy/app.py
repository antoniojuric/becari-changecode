import requests as rq

from flask import Flask, jsonify, request
from flask_cors import CORS

BASE_URL = 'http://52.233.158.172/change/api/'

REGISTER_URL = 'account/register'
LOGIN_URL = 'account/login'
TEAM_URL = 'team/details/24'
SUBMIT_URL = 'team/confirm?id=24&repository=https%3A%2F%2Fbitbucket.org%2Fantoniojuric%2Fbecari-changecode'


app = Flask(__name__)
CORS(app)


@app.route('/', methods=['POST'])
def proxy():
    url = request.headers['X-Proxy-URL']
    method = request.headers['X-Proxy-Method']
    token = request.headers.get('X-Authorization', None)

    headers = {'X-Authorization': token} if token is not None else {}

    if method == 'GET':
        resp = rq.get(url, headers=headers)
    elif method == 'POST':
        resp = rq.post(url, json=request.get_json(), headers=headers)

    return jsonify(resp.json()), resp.status_code


# HR API

BASE_URL_HR = BASE_URL + 'hr/'


@app.route('/hr/account/register', methods=['POST'])
def register_hr():
    resp = rq.post(BASE_URL_HR + 'account/register', json=request.get_json())
    return jsonify(resp.json()), resp.status_code


@app.route('/hr/account/login', methods=['POST'])
def login_hr():
    resp = rq.post(BASE_URL_HR + 'account/login', json=request.get_json())
    return jsonify(resp.json()), resp.status_code


@app.route('/hr/team/details/<int:id>', methods=['GET'])
def details_hr(id):
    token = request.headers.get('X-Authorization', None)
    headers = {'X-Authorization': token} if token is not None else {}

    resp = rq.get(BASE_URL_HR + 'team/details/%d' % id, headers=headers)
    return jsonify(resp.json()), resp.status_code


@app.route('/hr/team/confirm', methods=['GET'])
def submit_hr():
    token = request.headers.get('X-Authorization', None)
    headers = {'X-Authorization': token} if token is not None else {}

    resp = rq.get(BASE_URL_HR + 'team/confirm', headers=headers, params=request.args)
    return jsonify(resp.json()), resp.status_code


# EN API

BASE_URL_EN = BASE_URL + 'en/'


@app.route('/en/account/register', methods=['POST'])
def register_en():
    resp = rq.post(BASE_URL_EN + 'account/register', json=request.get_json())
    return jsonify(resp.json()), resp.status_code


@app.route('/en/account/login', methods=['POST'])
def login_en():
    resp = rq.post(BASE_URL_EN + 'account/login', json=request.get_json())
    return jsonify(resp.json()), resp.status_code


@app.route('/en/team/details/<int:id>', methods=['GET'])
def details_en(id):
    token = request.headers.get('X-Authorization', None)
    headers = {'X-Authorization': token} if token is not None else {}

    resp = rq.get(BASE_URL_EN + 'team/details/%d' % id, headers=headers)
    return jsonify(resp.json()), resp.status_code


@app.route('/en/team/confirm', methods=['GET'])
def submit_en():
    token = request.headers.get('X-Authorization', None)
    headers = {'X-Authorization': token} if token is not None else {}

    resp = rq.get(BASE_URL_EN + 'team/confirm', headers=headers, params=request.args)
    return jsonify(resp.json()), resp.status_code


if __name__ == '__main__':
    app.run('0.0.0.0', 8000, True)
