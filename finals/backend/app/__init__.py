from flask import Flask
from flask_cors import CORS

from .libs.west_db import WestSqliteDatabase

app = Flask(__name__)
app.config.from_object("app.config")

CORS(app)

db = WestSqliteDatabase("app/data/sqlite.db")

from .libs.west import West
from .libs.west_auth import JWT
from app.models import User

jwt = JWT(app, lambda payload: User.get(User.id == payload['user_id']))
west = West(app)

from app.handlers import *
from app.views import *
