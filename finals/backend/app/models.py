"""
    app.models
    ==========
    Definitions of data models and some operations with them.

    :copyright: (c) 2016 by Zvonimir Jurelinac
"""

import requests

from peewee import *
from werkzeug.security import check_password_hash

from .libs.west_db import WestModel
from .libs.west_exceptions import *
from .libs.west_auth import active_user, make_password_hash

from app import db


class BaseAppModel(WestModel):
    """Base model, specifies which database is to be used"""

    class Meta:
        database = db


class User(BaseAppModel):
    username = CharField(max_length=256, unique=True)
    password_hash = CharField(null=True)
    email = CharField(max_length=256, unique=True)

    __serialization__ = {
        'basic': ['id', 'username'],
        'full': ['id', 'username', 'email']
    }

    @classmethod
    def authenticate(cls, username, password):
        """Return user identified by given `username` and `password`"""
        try:
            user = cls.get(cls.username == username)
            if not check_password_hash(user.password_hash, password):
                raise AuthenticationError('Incorrect password provided.')
            return user
        except DoesNotExist as e:
            raise DoesNotExistError('User with username `%s` does not exist: [%s].'
                                    % (username, e))

    def change_password(self, old_password, new_password):
        """Change user account password, checking the old password first"""
        if not check_password_hash(self.password_hash, old_password):
            raise AuthenticationError('Incorrect old password provided.')
        self.password_hash = make_password_hash(new_password)
        self.save()


# Create model tables (if they don't yet exist)

MODELS = [User]
db.create_tables(MODELS, safe=True)
