from flask import Blueprint

from .libs.west_data import JSON
from .libs.west_auth import *

from app import app, west
from app.models import *
from app.helpers import *


west.register_validator('username', StringValidator(min_length=4, max_length=256))
west.register_validator('password', StringValidator(min_length=6))
west.register_validator('email', StringValidator(max_length=256, pattern=r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'))
west.register_validator('old_password', StringValidator(min_length=6))
west.register_validator('new_password', StringValidator(min_length=6))


@west.route(app, '/users', method='POST')
def register_user(username: str, email: str, password: str) -> Success:
    User.create(username=username, email=email, password_hash=make_password_hash(password))
    return 'User successfully created.', 201


@west.route(app, '/auth', method='POST')
def obtain_access_token(username: str, password: str) -> JSON:
    return generate_access_token(User.authenticate(username, password).id)


@west.route(app, '/profile/password', method='PUT', auth='JWT')
def change_password(old_password: str, new_password: str) -> Success:
    active_user().change_password(old_password, new_password)
    return 'Password successfully changed.'


@west.route(app, '/profile', method='GET', auth='JWT')
def get_profile() -> JSON(User, view='full'):
    return active_user()
