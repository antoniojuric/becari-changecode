"""
    west_helpers (east.helpers)
    ===========================
    Various helper functions and data structures

    :copyright: (c) 2016 by Zvonimir Jurelinac
    :license: MIT
"""

from collections import OrderedDict
from datetime import date, datetime


# Data serialization functions

def serialize(obj, *args):
    """Serialize an object for future JSON encoding"""
    value = obj(*args) if callable(obj) else obj
    return value.isoformat() if isinstance(value, (date, datetime)) else value


def to_jsondict(obj, view=''):
    """Convert Python object to JSON-encodable dictionary"""
    return obj.to_jsondict(view) if hasattr(obj, 'to_jsondict') else obj


def to_jsontype(type):
    """Convert Python type names to Javascript/JSON equivalents"""
    typename = type.__name__ if type else None
    renames = {'str': 'string', 'int': 'integer', 'bool': 'bool'}
    if typename in renames:
        typename = renames[typename]
    return typename


# Meta functions

def parse_argdict(extras):
    """Parse arguments dict - replace all functions by their return values"""
    return [(key, value() if callable(value) else value) for key, value in extras.items()]

def get_class_plural_name(cls):
    """Convert class name to it's plural form"""
    base = cls.__name__.lower()
    for ending in ('s', 'z', 'x', 'ch', 'sh'):
        if base.endswith(ending):
            return base + 'es'
    if base.endswith('y'):
        return base[:-1] + 'ies'
    else:
        return base + 's'


# Datastructures


class OrderedDefaultDict(OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and not callable(default_factory)):
            raise TypeError('first argument must be callable')
        super().__init__(*a, **kw)
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            return self.__missing__(key)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)

    def __deepcopy__(self, memo):
        import copy
        return type(self)(self.default_factory,
                          copy.deepcopy(self.items()))

    def __repr__(self):
        return 'OrderedDefaultDict(%s, %s)' % (self.default_factory,
                                               super().__repr__())
