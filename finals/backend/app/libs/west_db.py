"""
    west_db (east.db)
    =================
    WestModel class definition - extension of Peewee Model
    :copyright: (c) 2016 by Zvonimir Jurelinac
    :license: MIT
"""
import inspect

from peewee import *

from .west_exceptions import *
from .west_helpers import serialize, to_jsontype


class WestDatabase:
    exceptions = {
        'ConstraintError': IntegrityViolationError,
        'DatabaseError': DatabaseError,
        'DataError': DataError,
        'IntegrityError': IntegrityViolationError,
        'InterfaceError': DatabaseError,
        'InternalError': DatabaseError,
        'NotSupportedError': DatabaseError,
        'OperationalError': DatabaseError,
        'ProgrammingError': APIInternalError
    }


class WestModel(Model):
    """
    West extension of Peewee model
    Extends basic Peewee model by replacing builtin Peewee exceptions with
    those subclassed from BaseAPIException to allow easier error handling in
    REST APIs.
    Also, provides a `to_jsondict` method for direct model object
    serialization to JSON for returning responses from the API.
    """

    def to_jsondict(self, view=''):
        """Serialize the model instance to a JSON-encodable dictionary"""
        if hasattr(self, '__serialization__') and view:
            return {key: serialize(getattr(self, key), view)
                    for key in self.__serialization__[view]}
        else:
            return self._data


# Extensions of peewee database classes with West exceptions

class WestSqliteDatabase(WestDatabase, SqliteDatabase):
    pass


class WestMySQLDatabase(WestDatabase, MySQLDatabase):
    pass


class WestPostgresqlDatabase(WestDatabase, PostgresqlDatabase):
    pass
