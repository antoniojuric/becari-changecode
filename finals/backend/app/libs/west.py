"""
    west (east.module)
    ==================
    West class definition - West object allows defining API routes,
    validating request parameters and generating API documentation

    :copyright: (c) 2016 by Zvonimir Jurelinac
    :license: MIT
"""

import inspect
import os

from functools import wraps
from flask import request, send_from_directory

from .west_exceptions import *
from .west_auth import jwt_required


class West:
    def __init__(self, flask_app):
        """Create West object for the given `flask_app`"""
        self._flask_app = flask_app
        self._validators = {}
        self._routes = {}
        self._base_url = flask_app.config.get('BASE_API_URL', '/')

    def register_validator(self, param_name: str, param_validator):
        """Register parameter validator, for all API routes"""
        self._validators[param_name] = param_validator

    def route(self, base, url_rule: str, method: str = 'GET', auth: str = None):
        """
        API route decorator

        :param base:    Flask app or Blueprint object, on which the endpoint
                        should be added
        :param route:   URL route rule (can include path variables) for the given
                        endpoint
        :param method:  HTTP method accepted by the endpoint (default: GET)
        :param auth:    API authentication method, currently only 'JWT' is
                        supported
        """
        def decorator(f):

            if auth == 'JWT':
                f = jwt_required(f)

            params = [{
                'default': param.default,
                'type': param.annotation,
                'name': param.name,
                'auto_fill': True,
                'validator': self._validators.get(param.name, None)
            } if param.annotation is not inspect._empty else {
                'default': inspect._empty,
                'type': None,
                'name': param.name,
                'validator': None,
                'auto_fill': False
            } for param in inspect.signature(f).parameters.values()]

            self._routes[f] = {
                'auth': auth,
                'method': method,
                'endpoint': f,
                'params': params,
                'url_rule': url_rule,
                'return': f.__annotations__['return'] or None
            }

            @wraps(f)
            def decorated_function(*args, **parsed_params):
                for param in self._routes[f]['params']:
                    if param['auto_fill']:
                        raw_value = _get_request_param(param['name'])
                        if raw_value is None and param['default'] is inspect._empty:
                            raise MissingParameterError('Parameter `%s` is not present in the request' % param['name'])
                        try:
                            parsed_value = param['type'](raw_value) if raw_value is not None else param['default']
                            if param['validator'] is not None and raw_value is not None:
                                param['validator'](parsed_value)
                        except Exception as e:
                            raise BadParameterError('Parameter `%s` is invalid [%s]' % (param['name'], e))
                        parsed_params[param['name']] = parsed_value

                output, status = f(*args, **parsed_params), 200
                if isinstance(output, tuple):
                    output, status = output

                return self._routes[f]['return'].format(output), status

            base.add_url_rule(url_rule, f.__name__, decorated_function, methods=[method])

            return decorated_function
        return decorator


def _get_request_param(name: str):
    locations = [request.values, request.files]

    if request.method in ('PATCH', 'POST', 'PUT') and request.is_json:
        locations.append(request.get_json())

    for location in locations:
        if name in location:
            return location[name]
