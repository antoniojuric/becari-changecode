# Run this on virtual machine to install all needed stuff.

# IMPORTANT: To make sure that everyhing is working, logout from VM after this
# script finishes, and ssh again.

# Enable color bash.
sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' ~/.bashrc

# Install needed tools.
sudo apt-get update
sudo apt-get install \
  fish \
  htop

# Install Docker.
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo groupadd docker
sudo gpasswd -a $USER docker
docker run hello-world

# Clone repository.
cd ~/
git clone https://antoniojuric@bitbucket.org/antoniojuric/becari-changecode.git

# Build docker images.
cd ~/becari-changecode/finals/system/flask-base
./build.sh
cd ~/becari-changecode/finals/system/ng-base
./build.sh

# Start flask image.
cd ~/becari-changecode/finals/backend/
~/becari-changecode/finals/system/flask-base/run.sh serve
# Start ng image.
cd ~/becari-changecode/finals/frontend/
~/becari-changecode/finals/system/ng-base/run.sh serve
