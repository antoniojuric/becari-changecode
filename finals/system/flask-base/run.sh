#! /bin/bash

# Usage: run this script from the directory you want to mount as /code inside docker

docker stop flask-app 2>&1 > /dev/null 
docker rm 	flask-app 2>&1 > /dev/null 

if [ "$1" = "serve" ]; then
	docker run --name flask-app -v "$(pwd):/code" -p 8000:8000 -dt cc/flask-base python3 run.py
else
	docker run --name flask-app -v "$(pwd):/code" -p 8000:8000 -it cc/flask-base ash
fi
