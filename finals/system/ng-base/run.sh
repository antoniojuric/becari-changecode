#! /bin/bash

# Usage: run this script from the directory you want to mount as /code inside docker

docker stop ng-app 2>&1 > /dev/null 
docker rm 	ng-app 2>&1 > /dev/null 

if [ "$1" = "serve" ]; then
	docker run --name ng-app -v "$(pwd):/code" -p 4200:4200 -dt cc/ng-base ash -c "cd APP_NAME && ng serve --host 0.0.0.0"
else
	docker run --name ng-app -v "$(pwd):/code" -p 4200:4200 -it cc/ng-base ash
fi
